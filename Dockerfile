FROM python:3.10.2

ARG USER=gitlab-runner
ARG UID=1003
ARG DOCKER_GID=998

WORKDIR /updater
RUN addgroup --gid $UID $USER && \
    adduser --uid $UID --ingroup $USER --system $USER && \
    addgroup --gid $DOCKER_GID docker && \
    adduser $USER docker && \
    chown $USER:$USER /updater

COPY --chown=$USER:$USER requirements.txt .
RUN set -e; \
    rm -rf /var/lib/apt/lists/* && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg \
        lsb-release; \
    pip install --no-cache-dir -r requirements.txt; \
    apt-get remove -y build-essential libffi-dev libssl-dev && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/*;
COPY --chown=$USER:$USER . .

# Установка Docker
RUN apt-get update && apt-get install -y apt-transport-https ca-certificates curl gnupg lsb-release
RUN curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
RUN apt-get update && apt-get install -y docker-ce docker-ce-cli containerd.io

# Установка Docker Compose
RUN pip install docker-compose
RUN chmod +x /usr/local/bin/docker-compose

EXPOSE 5000
VOLUME /microservices
CMD uwsgi app.ini

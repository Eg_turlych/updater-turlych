""" Средство автоматического обновления сервисов """
import logging

import docker
from flask import Flask
from compose.cli.main import TopLevelCommand, project_from_options

app = Flask(__name__)

logging.basicConfig(format='%(asctime)s %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S',
                    level=logging.INFO)
flask_log = logging.getLogger('werkzeug')
flask_log.setLevel(logging.ERROR)


options = {'--detach': True,
           '--build': True,
           '--file': ['default.yml'],
           '--force-recreate': False,
           '--always-recreate-deps': False,
           '--no-recreate': False,
           '--abort-on-container-exit': False,
           '--remove-orphans': False,
           '--no-deps': False,
           '--no-build': False,
           '--scale': [],
           '--no-color': False,
            'no_start': True,
           'SERVICE': ['flask-example-turlych'],
           }
project = project_from_options('/microservices/servers', options)
dc = TopLevelCommand(project)
client = docker.from_env()

# pylint: disable=wrong-import-position
# pylint: disable=cyclic-import
# pylint: disable=import-self
from . import routes

import json
import logging
import os
import subprocess
import threading
from os import getenv

import docker
import requests
from flask import request
from compose.cli.command import get_project
from . import app, project, dc, client, options


def update_service(service: str, service_info: dict) -> None:
    """ Поток, обновляющий заданный сервис """
    repo_name = service_info[service]['repo_name']
    logging.info(f"Git pulling {service}[{repo_name}]")
    try:
        git_output = subprocess.check_output([
            "git",
            f"--git-dir=/microservices/{repo_name}/.git",
            f"--work-tree=/microservices/{repo_name}",
            "pull"
        ]).decode('utf-8').strip()
        logging.info(f"Git pull executed successfully for {service}")
        logging.info(f"Git pull output: {git_output}")

        options['SERVICE'] = [service]
        options['--context'] = service_info[service]['context']

        dc.pull(options=options)
        dc.up(options=options)

        logging.info(f"Service {service} is updated")
    except subprocess.CalledProcessError as e:
        logging.error(f"Git pull failed for {service} with exit code {e.returncode}")
        logging.error(f"Git pull output: {e.output.decode('utf-8')}")
    # requests.put(
    #     f'http://localhost/api/services/{service}/',
    #     headers={'Service-ID': "bb242e54-dab9-4a13-98ab-652092f9ef73"},
    #     json={'git_info': get_git_info(service_info)})
    get_git_info(service, service_info)


def get_git_info(service: str, service_info: dict) -> dict:
    """ Получение информации о git """
    repo_name = service_info[service]['repo_name']

    commit_message = subprocess.Popen(
        ['git', 'log', '--pretty=format:%s', '-1'], stdout=subprocess.PIPE,
        cwd=f"/microservices/{repo_name}"
    ).communicate()[0].decode('utf-8')
    date = subprocess.Popen(
        ['git', 'log', '--pretty=format:%at', '-1'], stdout=subprocess.PIPE,
        cwd=f"/microservices/{repo_name}"
    ).communicate()[0].decode('utf-8')

    service_info[service]["git_info"]["commit_message"]=commit_message
    service_info[service]["git_info"]["date"]=date
    with open("/microservices/auth/microservices.json", "w") as file:
        json.dump(service_info, file, indent=4)
    # return {
    #     'commit_message': commit_message,
    #     'date': int(date),
    #     'link': service_info['git_info']['link']
    # }


@app.route('/updater/<service>/', methods=['GET', 'POST'])
def service_updater(service: str):
    """ Webhook для автоматического обновления сервисов """
    logging.info(f'Request for update {service}')

    with open("/microservices/auth/microservices.json") as file:
        service_info = json.load(file)

    # service_info = requests.get(
    #     f'http://gateway/api/services/{service}/',
    #     headers={'Service-ID': getenv('SERVICE_ID')}
    # ).json()

    # secret_token = request.headers.get('X-Gitlab-Token')
    # if any(item is None for item in [secret_token, service_info]) or \
    #         'update_token' not in service_info or \
    #         secret_token != service_info['update_token']:
    #     logging.info(f'Bad request. Aborting update of {service}...')
    #     return 'Bad request', 400

    logging.info(f'Launched update of {service}')
    threading.Thread(target=update_service, args=(service, service_info)).start()

    return 'Accepted', 202


@app.route('/updater/status/')
def status():
    """ Обработка проверки статуса сервиса """
    return {'status': 'ok'}, 200
